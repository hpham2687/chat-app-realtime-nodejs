let userAvatar = null;
let userInfo = {};
let originAvatarSrc = null;
let originUserInfo = {};
let userInfoPassword = {};

function updateUserInfo() {
  //change password tab

  let oldPassword = $("input[name='current_password']");
  let newPassword = $("input[name='new_password']");
  let newPasswordConfirm = $("input[name='confirm_new_password']");

  oldPassword.bind("change", function (e) {
    userInfoPassword.current_password = $(this).val();
    console.log(userInfoPassword);
  });
  newPassword.bind("change", function (e) {
    userInfoPassword.new_password = $(this).val();
    console.log(userInfoPassword);
  });
  newPasswordConfirm.bind("change", function (e) {
    userInfoPassword.confirm_new_password = $(this).val();
    console.log(userInfoPassword);
  });

  //general tab
  $("#input-change-avatar").bind("change", function (e) {
    let file = $(this).prop("files")[0];
    let match = ["image/png", "image/jpg", "image/jpeg"];
    let limit = 1048576; //byte = 1mb
    if ($.inArray(file.type, match) == -1) {
      //alert sai type
      alertify.notify("Kieu file ko hop le", "error", 7);
      $(this).val(null);
      return false;
    }
    if (file.size > limit) {
      //alert sai type
      alertify.notify("File phai < 1Mb", "error", 7);
      $(this).val(null);
      return false;
    }
    if (typeof FileReader != undefined) {
      var reader = new FileReader();
      reader.onload = function () {
        var dataURL = reader.result;
        let img = $("#img-preview");
        img.attr("src", dataURL);
      };
      let formData = new FormData();
      formData.append("avatar", file);
      userAvatar = formData;

      reader.readAsDataURL(file);
    } else {
      alertify.notify(
        "FileReader trinh duyet ko ho tro file reader",
        "error",
        7
      );
      $(this).val(null);
      return false;
    }
  });

  $("#edit-username").bind("change", function (e) {
    userInfo.userName = $(this).val();
  });
  $("#edit-male-gender").bind("click", function (e) {
    userInfo.gender = $(this).val();
  });
  $("#edit-female-gender").bind("click", function (e) {
    userInfo.gender = $(this).val();
  });
  $("#edit-address").bind("change", function (e) {
    userInfo.address = $(this).val();
  });
  $("#edit-phone").bind("change", function (e) {
    userInfo.phone = $(this).val();
  });
}

function callUpdateUserInfo() {
  fetch("/user/update-info", {
    method: "PUT",
    body: JSON.stringify(userInfo),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((result) => {
      if (!result.message) throw new Error(result);
      console.log("Successdfdf:", result);

      // display suceess alert
      $(".personal-info .alert-success").eq(0).css("display", "block");
      $(".personal-info .alert-success")
        .eq(0)
        .find("span")
        .text(result.message);

      // update origin userInfo
      originUserInfo = Object.assign(originUserInfo, userInfo);
      console.log(originUserInfo);
      // update username at navbar
      $(".username-text").text(originUserInfo.userName);

      $("#cancel-edit-user-info").click();
    })
    .catch((error) => {
      console.log(error);
      console.log("Error:", error.message);
      // display error
      $(".personal-info .alert-danger").eq(0).css("display", "block");
      // change error text
      $(".personal-info .alert-danger").eq(0).find("span").text(error.message);
      $("#cancel-edit-user-info").click();
    });
}
function callUpdatePassword() {
  //validate before send
  if (
    userInfoPassword.current_password < 6 ||
    userInfoPassword.new_password.length < 6 ||
    userInfoPassword.confirm_new_password.length < 6
  ) {
    alertify.notify("password phai >6", "error", 7);
  }
  if (userInfoPassword.new_password != userInfoPassword.confirm_new_password) {
    alertify.notify("password not match", "error", 7);
  }

  //send put data
  console.log(userInfoPassword);
  fetch("/user/update-password", {
    method: "PUT",
    body: JSON.stringify(userInfoPassword),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((result) => {
      console.log(result);

      if (result.errors) {
        let errMsg = "";
        console.error("Error:", result.errors);
        for (let i = 0; i < result.errors.length; i++) {
          errMsg += `${result.errors[i].msg}<br>`;
        }
        // display error
        $("#password-profile .alert-danger").eq(0).css("display", "block");
        // change error text
        $("#password-profile .alert-danger").eq(0).find("span").text(errMsg);
        return 0;
      }

      $("#password-profile .alert-danger").eq(0).css("display", "none");

      $("#password-profile .alert-success").eq(0).css("display", "block");
      $("#password-profile .alert-success")
        .eq(0)
        .find("span")
        .html(result.message);
    })
    .catch((error) => {
      console.error("Error:", error);

      // display error
      $("#password-profile .alert-danger").eq(0).css("display", "block");
      // change error text
      $("#password-profile .alert-danger")
        .eq(0)
        .find("span")
        .text(error.responseText);
    });
}

function callUpdateAvatar() {
  fetch("/user/update-avatar", {
    method: "PUT",
    body: userAvatar,
  })
    .then((response) => response.json())
    .then((result) => {
      $(".personal-info .alert-success").eq(0).css("display", "block");
      $(".personal-info .alert-success")
        .eq(0)
        .find("span")
        .text(result.message);

      //update avatar navbar
      originAvatarSrc = result.imgSrc;
      $("#nav-avatar").attr("src", originAvatarSrc);
      $("#cancel-edit-user-info").click();

      console.log("Success:", result);
    })
    .catch((error) => {
      console.error("Error:", error);

      // display error
      $(".personal-info .alert-danger").eq(0).css("display", "block");
      // change error text
      $(".personal-info .alert-danger")
        .eq(0)
        .find("span")
        .text(error.responseText);
    });
}
$(document).ready(function () {
  // listen to field change
  updateUserInfo();

  // origin data
  originAvatarSrc = $("#img-preview").attr("src");
  originUserInfo = {
    userName: $("#edit-username").val(),
    gender: $("#edit-male-gender").is(":checked") ? "Male" : "Female",
    address: $("#edit-address").val(),
    phone: $("#edit-phone").val(),
  };

  //submit

  // save edit password
  $("#save-edit-user-password").bind("click", function (e) {
    if ($.isEmptyObject(userInfoPassword)) {
      alertify.notify("Ban phai thay doi truoc khi an cap nhat", "error", 7);
    } else {
      //call update password
      callUpdatePassword();
    }
  });

  //cancel edit password

  $("#cancel-edit-user-password").bind("click", function (e) {
    userInfoPassword = {};
    $("input[name='current_password']").val("");
    $("input[name='new_password']").val("");
    $("input[name='confirm_new_password']").val("");
  });

  //save edit info
  $("#save-edit-user-info").bind("click", function (e) {
    if ($.isEmptyObject(userInfo) && !userAvatar) {
      alertify.notify("Ban phai thay doi truoc khi an cap nhat", "error", 7);
      return false;
    }
    if (userAvatar) {
      callUpdateAvatar();
    }
    if (!$.isEmptyObject(userInfo)) {
      console.log(userInfo);

      callUpdateUserInfo();
    }

    console.log(userAvatar);
  });
  $("#cancel-edit-user-info").bind("click", function (e) {
    //userInfo = originUserInfo;
    userInfo = {};
    //console.log(originUserInfo);
    $("#edit-username").val(originUserInfo.userName);
    originUserInfo.gender === "Male"
      ? ($("#edit-male-gender")[0].checked = true)
      : ($("#edit-female-gender")[0].checked = true);
    $("#edit-address").val(originUserInfo.address);
    $("#edit-phone").val(originUserInfo.phone);
    userAvatar = null;
    $("#img-preview").attr("src", originAvatarSrc);
    $("#input-change-avatar").val(null);
  });
});
