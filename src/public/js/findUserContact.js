const KEYCODE = {
  ENTER: 13,
};

function callFindUser(element) {
  if (element.which === KEYCODE.ENTER || element.type === "click") {
    let userInput = $("#find-user-input");

    let keyword = userInput.val();
    console.log(keyword);
    //validate
    if (!keyword.length) {
      alertify.notify("Ban phai nhap noi dung vao!", "error", 7);
      return false;
    }
    //request list user
    fetch(`/contact/findUsers/${keyword}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    })
      .then((res) => res.text())
      .then((result) => {
        console.log(result.body);
        $("#find-newfriend").html(result);
      });
  }
}
$(document).ready(function () {
  let userInput = $("#find-user-input");
  let userSubmitSearch = $("#find-user-btn");
  userInput.bind("keypress", callFindUser);
  userSubmitSearch.bind("click", callFindUser);
});
