export const authMiddleware = {
  isLoggedOut: (req, res, next) => {
    if (!req.isAuthenticated()) {
      console.log("vao day out");
      // method of passport
      return res.redirect("/auth");
    }
    next();
  },
  isLoggedIn: (req, res, next) => {
    if (req.isAuthenticated()) {
      // method of passport
      return res.redirect("/");
    }
    next();
  },
};
