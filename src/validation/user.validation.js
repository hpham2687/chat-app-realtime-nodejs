import { check } from "express-validator";
import { transValidation } from "./../../lang/vi";

let updateInfo = [
  check("userName", "user name 3->17").optional().isLength({ min: 3, max: 17 }),
  check("gender", "chua chon gioi tinh").optional().isIn(["male", "femail"]),
  check("address", "30 ki tu, min 3").optional().isLength({ min: 3, max: 20 }),
  check("phone", "sai dinh dang")
    .optional()
    .matches(/(09|01[2|6|8|9])+([0-9]{8})\b/),
];
let updatePassword = [
  check("current_password", "current passs sai dinh dang <6").isLength({
    min: 6,
  }),
  check("new_password", "current passs sai dinh dang <6")
    .isLength({ min: 6 })
    .exists(),
  check("confirm_new_password", "confirm ko trung")
    .isLength({ min: 6 })
    .custom((value, { req }) => {
      console.log(req.body);
      return value === req.body.new_password;
    }),
];
module.exports = {
  updateInfo,
  updatePassword,
};
