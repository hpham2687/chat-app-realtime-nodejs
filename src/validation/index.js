import authValidation from "./authValidation";
import userValidation from "./user.validation";

module.exports = {
  authValid: authValidation,
  userValid: userValidation,
};
