import express from "express";
import passport from "passport";
import { authValid, userValid } from "./../validation";
import authController from "./../controllers/auth.controller";
import userController from "./../controllers/user.controller";
import { authMiddleware } from "./../middlewares/auth.middleware";

let userRoute = express.Router();
userRoute.get("/logout", authController.logout);
userRoute.post("/register", authValid.register, authController.register);
userRoute.put(
  "/update-avatar",
  authMiddleware.isLoggedOut,
  userController.updateAvatar
);
userRoute.put(
  "/update-password",
  authMiddleware.isLoggedOut,
  userValid.updatePassword,
  userController.updatePassword
);

userRoute.put(
  "/update-info",
  authMiddleware.isLoggedOut,
  userValid.updateInfo,
  userController.updateInfo
);
userRoute.post(
  "/login",
  authMiddleware.isLoggedIn,
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/auth",
    successFlash: true,
    failureFlash: true,
  })
);
// GET /user/loginFacebook
userRoute.get(
  "/loginFacebook",
  authMiddleware.isLoggedIn,
  passport.authenticate("facebook", {
    scope: ["email"],
  })
);
// GET /user/loginFacebook/callback
userRoute.get(
  "/loginFacebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "/",
    failureRedirect: "/auth",
  })
);
// GET /user/loginGoogle
userRoute.get(
  "/loginGoogle",
  authMiddleware.isLoggedIn,
  passport.authenticate("google", {
    scope: ["email", "profile"],
  })
);
// GET /user/loginGoogle/callback
userRoute.get(
  "/loginGoogle/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/auth",
  })
);
module.exports = userRoute;
