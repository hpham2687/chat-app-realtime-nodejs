import express from "express";
import contactController from "./../controllers/contact.controller";
import { authMiddleware } from "./../middlewares/auth.middleware";

let userRoute = express.Router();
userRoute.get(
  "/findUsers/:keyword",
  authMiddleware.isLoggedOut,
  contactController.findUsers
);

module.exports = userRoute;
