import express from "express";
import homeController from "./../controllers/home.controller";
import authController from "./../controllers/auth.controller";
import initPassportLocal from "./../controllers/passport.controller/local";
import initPassportFacebook from "./../controllers/passport.controller/facebook";
import initPassportGoogle from "./../controllers/passport.controller/google";
import { authMiddleware } from "./../middlewares/auth.middleware";
import UserRoute from "./user.route";
import ContactRoute from "./contact.route";
let router = express.Router();
// Init all passport
initPassportLocal();
initPassportFacebook();
initPassportGoogle();

/**
 * Init all route
 * @param app from exactly express module
 */

let initRoutes = (app) => {
  router.get("/", authMiddleware.isLoggedOut, homeController.getHomeRoute);

  //login/register page
  router.get("/auth", authMiddleware.isLoggedIn, authController.getAuthRoute);
  // define vertification route
  router.get("/vertify/:activationToken", authController.activateEmail);
  // user route
  app.use("/user", UserRoute);
  //contact route
  app.use("/contact", ContactRoute);

  return app.use("/", router);
};

module.exports = initRoutes;
