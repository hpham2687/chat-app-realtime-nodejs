const homeController = {
  getHomeRoute: async (req, res) => {
    res.render("main/home/home", {
      success: req.flash("success"),
      user: req.user,
    });
  },
};
module.exports = homeController;
