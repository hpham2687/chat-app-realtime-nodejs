import { validationResult } from "express-validator";
import { registerService, emailJwtVertificationService } from "./../services";

const authController = {
  getAuthRoute: async (req, res) => {
    res.render("auth/auth", {
      errors: req.flash("errors"),
      success: req.flash("success"),
    });
  },
  register: async (req, res) => {
    let errorArr = [];
    let successArr = [];
    /**
     * validate input with "express-validator" (check length, check email regex...)
     */
    const ValidationError = validationResult(req);
    if (!ValidationError.isEmpty()) {
      let errors = Object.values(ValidationError.mapped());
      errorArr = errors.map((item) => item.msg);
      req.flash("errors", errorArr);
      return res.redirect("/auth");
    }

    /**
     * validate email (isActive, isDeleted) in database & create
     */
    const { email, gender, password } = req.body;
    try {
      const createUserSuccess = await registerService(email, gender, password);
      successArr.push(createUserSuccess);
      req.flash("success", successArr);
      return res.redirect("/auth");
    } catch (error) {
      errorArr.push(error);
      req.flash("errors", errorArr);
      return res.redirect("/auth");
    }
  },
  activateEmail: async (req, res) => {
    try {
      const { activationToken } = req.params;
      let resultVertification = await emailJwtVertificationService(
        activationToken
      );

      res.json({
        msg: "verify thanh cong" + JSON.stringify(resultVertification),
      });
    } catch (error) {
      res.json({ error: error });
    }
  },
  logout: async (req, res) => {
    req.logout(); // remove session passport user
    req.flash("success", "dang xuat thanh cong");
    res.redirect("/auth");
  },
};
module.exports = authController;
