import passport from "passport";
import passportGoogle from "passport-google-oauth2";
import UserModel from "../../models/user.model";
let GoogleStrategy = passportGoogle.Strategy;

/**
 * Valid user account type : Google
 */
let initPassportGoogle = () => {
  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.GG_APP_ID,
        clientSecret: process.env.GG_APP_SECRET,
        callbackURL: process.env.GG_APP_CALLBACK_URL,
        profileFields: ["email", "gender", "displayName"],
        passReqToCallback: true,
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          let user = await UserModel.findByGoogleUid(profile.id);
          console.log("vao day");
          if (user) {
            return done(
              null,
              user,
              req.flash("success", "dang nhap thanh cong")
            );
          }
          console.log(profile);
          let newUser2Save = {
            userName: profile.displayName,
            gender: profile.gender,
            local: {
              isActive: true,
            },
            google: {
              uid: profile.id,
              token: accessToken,
              email: profile.emails[0].value,
            },
          };
          let newUser = await UserModel.createNew(newUser2Save);
          return done(
            null,
            newUser,
            req.flash(
              "success",
              "dang ky xong roi, dang nhap thanh cong" + newUser.userName
            )
          );
        } catch (error) {
          console.log(error);
          return done(null, false, req.flash("errors", "500 error"));
        }
      }
    )
  );
  // save userID to session
  passport.serializeUser((user, done) => {
    done(null, user._id);
  });
  passport.deserializeUser((id, done) => {
    UserModel.findUserById(id)
      .then((user) => {
        return done(null, user);
      })
      .catch((error) => {
        return done(error, null);
      });
  });
};
module.exports = initPassportGoogle;
