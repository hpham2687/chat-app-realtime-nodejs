import passport from "passport";
import passportLocal from "passport-local";
import UserModel from "../../models/user.model";
let localStrategy = passportLocal.Strategy;

/**
 * Valid user account type : local
 */
let initPassportLocal = () => {
  passport.use(
    new localStrategy(
      {
        usernameField: "email", // email === name attribute in html
        passwordField: "password",
        passReqToCallback: true,
      },
      async (req, email, password, done) => {
        try {
          let user = await UserModel.findByEmail(email);
          if (!user) {
            return done(null, false, req.flash("errors", "ko tim thay email"));
          }
          console.log(user);
          if (!user.local.isActive) {
            return done(
              null,
              false,
              req.flash("errors", "account chua active")
            );
          }
          let checkPassword = await user.comparePassword(password);
          if (!checkPassword) {
            return done(null, false, req.flash("errors", "sai password"));
          }
          return done(null, user, req.flash("success", "dang nhap thanh cong"));
        } catch (error) {
          console.log(error);
          return done(null, false, req.flash("errors", "500 error"));
        }
      }
    )
  );
  // save userID to session
  passport.serializeUser((user, done) => {
    done(null, user._id);
  });
  passport.deserializeUser((id, done) => {
    UserModel.findUserById(id)
      .then((user) => {
        return done(null, user);
      })
      .catch((error) => {
        return done(error, null);
      });
  });
};
module.exports = initPassportLocal;
