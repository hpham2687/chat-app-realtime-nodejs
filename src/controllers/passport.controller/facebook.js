import passport from "passport";
import passportFacebook from "passport-facebook";
import UserModel from "../../models/user.model";
let FacebookStrategy = passportFacebook.Strategy;

/**
 * Valid user account type : Facebook
 */
let initPassportFacebook = () => {
  passport.use(
    new FacebookStrategy(
      {
        clientID: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        callbackURL: process.env.FACEBOOK_APP_CALLBACK_URL,
        profileFields: ["email", "gender", "displayName"],
        passReqToCallback: true,
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          let user = await UserModel.findByFacebookUid(profile.id);
          console.log("vao day");
          if (user) {
            return done(
              null,
              user,
              req.flash("success", "dang nhap thanh cong")
            );
          }
          console.log(profile);
          let newUser2Save = {
            userName: profile.displayName,
            gender: profile.gender,
            local: {
              isActive: true,
            },
            facebook: {
              uid: profile.id,
              token: accessToken,
              email: profile.emails[0].value,
            },
          };
          let newUser = await UserModel.createNew(newUser2Save);
          return done(
            null,
            newUser,
            req.flash(
              "success",
              "dang ky xong roi, dang nhap thanh cong" + newUser.userName
            )
          );
        } catch (error) {
          console.log(error);
          return done(null, false, req.flash("errors", "500 error"));
        }
      }
    )
  );
  // save userID to session
  passport.serializeUser((user, done) => {
    done(null, user._id);
  });
  passport.deserializeUser((id, done) => {
    UserModel.findUserById(id)
      .then((user) => {
        return done(null, user);
      })
      .catch((error) => {
        return done(error, null);
      });
  });
};
module.exports = initPassportFacebook;
