import multer from "multer";
import { v4 as uuidv4 } from "uuid";
import fs from "fs-extra";
import { userService } from "./../services";
import { validationResult } from "express-validator";

let storageAvatar = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "src/public/images/users");
  },
  filename: (req, file, callback) => {
    let match = ["image/png", "image/jpg", "image/jpeg"];
    if (match.indexOf(file.mimetype) === -1) {
      return callback("ko phai la anh", null);
    }

    let avatarName = `${Date.now()}-${uuidv4()}-${file.originalname}`;
    callback(null, avatarName);
  },
});

let avatarUpdloadFileMulter = multer({
  storage: storageAvatar,
  limits: {
    fieldSize: 20,
  },
}).single("avatar");

const userController = {
  updateInfo: async (req, res) => {
    let errorArr = [];
    /**
     * validate input with "express-validator" (check length, check user, phone regex...)
     */
    const ValidationError = validationResult(req);
    if (!ValidationError.isEmpty()) {
      let errors = Object.values(ValidationError.mapped());
      errorArr = errors.map((item) => item.msg);

      return res.status(500).send(errorArr);
    }
    try {
      let updateUserItem = req.body;
      console.log(updateUserItem);
      await userService.updateUser(req.user._id, updateUserItem);
      let result = {
        message: "update thong tin thanh cong",
      };
      res.status(200).send(result);
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  },
  updatePassword: async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      if (
        !req.body.current_password ||
        !req.body.new_password ||
        !req.body.confirm_new_password
      ) {
        res.status(400).send("empty field");
        return;
      }
      const updateUserPassword = await userService.updateUserPassword(
        req.body.current_password,
        req.body.new_password,
        req.user
      );
      if (updateUserPassword === "S") {
        res.status(400).send("Sai password cu");
        return;
      }
      let result = {
        message: "Update password thanh cong",
      };
      res.status(200).send(result);
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  },
  updateAvatar: async (req, res) => {
    avatarUpdloadFileMulter(req, res, async (error) => {
      if (error) {
        console.log(error);
        if (error.message) {
          return res.status(500).send("Size anhq ua lon");
        }
        return res.status(500).send(error);
      }
      try {
        let updateUserItem = {
          avatar: req.file.filename,
          updatedAt: Date.now(),
        };
        let userUpdated = await userService.updateUser(
          req.user._id,
          updateUserItem
        );

        //remove old avatar
        await fs.remove(`src/public/images/users/${userUpdated.avatar}`);

        let result = {
          message: "update anh dai dien thanh cong",
          imgSrc: `/images/users/${req.file.filename}`,
        };
        return res.status(200).send(result);
      } catch (error) {}
      return res.status(500).send(error);
    });
  },
};
module.exports = userController;
