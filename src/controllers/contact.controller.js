import { contactService } from "./../services";
const contactController = {
  findUsers: async (req, res) => {
    try {
      let userId = req.user._id;
      let { keyword } = req.params;

      let users = await contactService.findUsers(userId, keyword);
      console.log(users);
      return res.render("main/modals/sections/listContacts", { users });
      //finuser
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  },
};
module.exports = contactController;
