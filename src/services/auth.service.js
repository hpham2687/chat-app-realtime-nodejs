/**
 * auth.service.js - Handle logic
 */
import UserModel from "./../models/user.model";
import bcrypt from "bcrypt";
import { transErrors, transSuccess } from "./../../lang/vi";
import { sendMail } from "./../config/mailer";
import { generateToken, verifyToken } from "./../helpers/jwt.helper";
let saltRounds = 4;
let register = (email, gender, password) => {
  return new Promise(async (resolve, reject) => {
    /**
     * check email Exist, check deleted, check isActive (isVerified)
     */
    let isEmailExist = await UserModel.findByEmail(email);
    if (isEmailExist) {
      if (isEmailExist.deletedAt !== null) {
        return reject(transErrors.account_removed);
      }
      if (!isEmailExist.local.isActive) {
        return reject(transErrors.account_unactive);
      }
      return reject(transErrors.account_in_user);
    }

    /**
     * create & save to mongodb
     */
    let salt = bcrypt.genSaltSync(saltRounds);
    let userItem = {
      userName: email.split("@")[0],
      gender,
      local: {
        email,
        password: bcrypt.hashSync(password, salt),
      },
    };

    //create activation token
    let activationToken = await generateToken(
      userItem,
      process.env.JWT_ACTIVATION_TOKEN_SECRET,
      "5m"
    );
    await UserModel.createNew(userItem);

    sendMail(
      email,
      "Vertification account",
      `<h1>click to verify <a href="http://localhost:3000/vertify/${activationToken}">this link</h1>`
    )
      .then((success) => {
        console.log(success);
        resolve(transSuccess.userCreated(email));
      })
      .catch(async (error) => {
        console.log(error);
        await UserModel.removeById(user._id);
        reject("send email vertifition faild");
      });
  });
};

let emailJwtVertification = (activationToken) => {
  return new Promise(async (resolve, reject) => {
    try {
      //jwt verify token
      const user = await verifyToken(
        activationToken,
        process.env.JWT_ACTIVATION_TOKEN_SECRET
      );
      //check isExist && isActive
      let isEmailExist = await UserModel.findByEmail(user.local.email);
      if (isEmailExist && isEmailExist.local.isActive) {
        console.log("is activ=", isEmailExist.local.isActive);
        resolve("This email already actived");
      }
      // change isActive -> true
      isEmailExist.local.isActive = true;
      await isEmailExist.save();

      resolve(user);
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};
module.exports = {
  register,
  emailJwtVertification,
};
