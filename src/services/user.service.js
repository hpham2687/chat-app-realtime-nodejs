import { resolve } from "bluebird";
import UserModel from "./../models/user.model";
import bcrypt from "bcrypt";
let saltRounds = 4;

/**
 * Update user Info
 * @param {userId} id
 * @param {dataUpdate} item
 */
let updateUser = (id, item) => {
  return UserModel.updateUser(id, item);
};
let updateUserPassword = async (currentPassword, newPassword, user) => {
  let salt = bcrypt.genSaltSync(saltRounds);

  console.log(currentPassword, newPassword);

  let checkPassword = await user.comparePassword(currentPassword);
  if (!checkPassword) {
    return "S";
  } else {
    return UserModel.findAndModify(
      user._id,
      bcrypt.hashSync(newPassword, salt)
    );
  }
};

module.exports = {
  updateUser,
  updateUserPassword,
};
