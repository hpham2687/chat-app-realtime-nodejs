import { register, emailJwtVertification } from "./auth.service";
import userService from "./user.service";
import contactService from "./contact.service";

module.exports = {
  registerService: register,
  emailJwtVertificationService: emailJwtVertification,
  userService,
  contactService,
};
