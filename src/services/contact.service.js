import ContactModel from "./../models/contact.model";
import UserModel from "./../models/user.model";
import _ from "lodash";
const findUsers = async (userId, keyword) => {
  //finuser
  console.log("vao contact service");
  return new Promise(async (resolve, reject) => {
    let deprecatedUserIds = [];
    let contactsByUser = await ContactModel.findAllByUser(userId);
    //console.log("vao contact service3");
    //console.log("contactsByUser= ", contactsByUser);
    contactsByUser.forEach((contact) => {
      deprecatedUserIds.push(contact.userId);
      deprecatedUserIds.push(contact.contactId);
    });
    deprecatedUserIds = _.uniqBy(deprecatedUserIds);
    //console.log(deprecatedUserIds);
    let users2Show = await UserModel.findAllForAddContact(
      deprecatedUserIds,
      keyword
    );
    resolve(users2Show);
  });
};

module.exports = {
  findUsers,
};
