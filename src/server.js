//let express = require("express");
import express from "express";
import connectDb from "./config/mongodb";
import configViewEngine from "./config/viewEngine";
import bodyParser from "body-parser";
import initRoutes from "./routes";
import connectFlash from "connect-flash";
import configSession from "./config/session";
import passport from "passport";
import fs from "fs";
import path from "path";
import https from "https";
require("dotenv").config();

// let app = express();
// const PORT = process.env.PORT || 3000;
// //connect to mongodb
// connectDb();
// // config session
// configSession(app);
// //config view engine
// configViewEngine(app);
// // enable post data for request
// app.use(bodyParser.urlencoded({ extended: true }));
// //enable flash message
// app.use(connectFlash());
// //config passport
// app.use(passport.initialize());
// app.use(passport.session());
// //init all routes
// initRoutes(app);

// https
//   .createServer(
//     {
//       key: fs.readFileSync(path.join(__dirname, "openssl", "server.key")),
//       cert: fs.readFileSync(path.join(__dirname, "openssl", "server.cert")),
//     },
//     app
//   )
//   .listen(PORT, () => {
//     console.log("running on port ", PORT);
//   });

let app = express();
const PORT = process.env.PORT || 3000;
//connect to mongodb
connectDb();
// config session
configSession(app);
//config view engine
configViewEngine(app);
// enable post data for request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//enable flash message
app.use(connectFlash());
//config passport
app.use(passport.initialize());
app.use(passport.session());
//init all routes
initRoutes(app);

app.listen(PORT, () => {
  console.log("running on port ", PORT);
});
