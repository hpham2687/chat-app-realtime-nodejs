import mongoose from "mongoose";
import bcrypt from "bcrypt";
let UserSchema = new mongoose.Schema({
  userName: String,
  gender: {
    type: String,
    default: "Male",
  },
  phone: { type: String, default: null },
  address: { type: String, default: null },
  avatar: { type: String, default: "avatar-default.jpg" },
  role: { type: String, default: "user" },
  local: {
    email: { type: String, trim: true },
    password: String,
    isActive: { type: Boolean, default: false },
  },
  facebook: {
    uid: String,
    token: String,
    email: { type: String, trim: true },
  },
  google: {
    uid: String,
    token: String,
    email: { type: String, trim: true },
  },
  createdAt: { type: Number, default: Date.now },
  updatedAt: { type: Number, default: null },
  deletedAt: { type: Number, default: null },
});

UserSchema.statics = {
  createNew(item) {
    return this.create(item);
  },
  findByEmail(em) {
    return this.findOne({ "local.email": em }, (err, res) => {});
  },
  findUserById(id) {
    return this.findById(id).exec();
  },
  findByFacebookUid(uid) {
    return this.findOne({ "facebook.uid": uid }, (err, res) => {});
  },
  findByGoogleUid(uid) {
    return this.findOne({ "google.uid": uid }, (err, res) => {});
  },
  updateUser(id, item) {
    return this.findByIdAndUpdate(id, item).exec();
    //return old data when update success
  },
  findAndModify(id, password) {
    return this.findById(id, function (err, doc) {
      if (err) console.log(err);
      doc.local.password = password;
      doc.save();
    });
  },
  removeById(id) {
    return this.findByIdAndRemove(id, function (err, category) {
      if (err) {
        console.log(err);
      }
    });
  },
  /**
   * find all items that match the keyword, filter decrepted user
   * @param {array} deprecatedUserIds
   * @param {string} keyword
   */
  findAllForAddContact(deprecatedUserIds, keyword) {
    return this.find(
      {
        $and: [
          {
            _id: {
              $nin: deprecatedUserIds,
            },
          },
          { "local.isActive": true },
          {
            $or: [
              {
                userName: {
                  $regex: keyword,
                },
              },
              {
                "local.email": {
                  $regex: keyword,
                },
              },
              {
                "facebook.email": {
                  $regex: keyword,
                },
              },
              {
                "google.email": {
                  $regex: keyword,
                },
              },
            ],
          },
        ],
      },
      { _id: 1, userName: 1, address: 1, avatar: 1 }
    ).exec();
  },
};

UserSchema.methods = {
  comparePassword(password) {
    return bcrypt.compare(password, this.local.password);
  },
};
module.exports = mongoose.model("user", UserSchema);
