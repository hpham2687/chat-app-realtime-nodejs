import mongoose from "mongoose";

let ContactSchema = new mongoose.Schema({
  userId: String,
  contactId: String,
  status: { type: Boolean, default: false },
  createdAt: { type: Number, default: Date.now },
  updatedAt: { type: Number, default: null },
  deletedAt: { type: Number, default: null },
});

ContactSchema.statics = {
  createNew(item) {
    return this.create(item);
  },
  /**
   * Find all items that related with user (user id, usercontact <-->)
   * @param {string} userId
   */
  findAllByUser(userId) {
    // userId = userId.toString();
    let userIdString = userId.toHexString();
    //return this.find({}).exec();
    return this.find(
      {
        $or: [{ userId: userIdString }, { contactId: userIdString }],
      },
      function (err, docs) {
        if (!err) console.log(err);
      }
    ).exec();
  },
};
module.exports = mongoose.model("contact", ContactSchema);
