import mongoose from "mongoose";
import bluebird from "bluebird";

require("dotenv").config();

// connect to mongo db
const connectDb = () => {
  mongoose.Promise = bluebird;

  const DB_NAME = process.env.DB_NAME || "chatapp";
  const DB_PASSWORD = process.env.DB_PASSWORD || "admin";

  const URI = `mongodb+srv://admin:${DB_PASSWORD}@cluster0.fzodq.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`;
  return mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
};

module.exports = connectDb;
