import nodemailer from "nodemailer";
require("dotenv").config();
const email = process.env.EMAIL_ACCOUNT;
const password = process.env.EMAIL_PASSWORD;
const mailHost = process.env.MAIL_HOST;
const mailPort = process.env.PORT;

console.log(email, password);
// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  host: mailHost,
  port: mailPort,
  secure: false, // true for 465, false for other ports
  auth: {
    user: email, // generated ethereal user
    pass: password, // generated ethereal password
  },
});

// send mail with defined transport object
export const sendMail = async (to, subject, htmlContent) => {
  let info = await transporter.sendMail({
    from: email, // sender address
    to: to, // list of receivers
    subject: "Hello ✔", // Subject line
    html: htmlContent, // html body
  });
  return info;
  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
};
