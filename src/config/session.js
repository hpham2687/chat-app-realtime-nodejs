import session from "express-session";
import connectMongo from "connect-mongo";

const DB_NAME = process.env.DB_NAME || "chatapp";
const DB_PASSWORD = process.env.DB_PASSWORD || "admin";

const URI = `mongodb+srv://admin:${DB_PASSWORD}@cluster0.fzodq.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`;

let MongoStore = connectMongo(session);

/**
 * sessionStore use to save session to mongodb
 */
let sessionStore = new MongoStore({
  url: URI,
  autoReconnect: true,
  autoRemove: "native",
});
/**
 * Config session for app
 * @param app from exactly express module
 */
let configSession = (app) => {
  app.use(
    session({
      key: "express.sid",
      secret: "mySecret",
      store: sessionStore,
      resave: true,
      saveUninitialized: false,
      cookie: {
        maxAge: 1000 * 60 * 60 * 24, //86400000 = 1 day
      },
    })
  );
};

module.exports = configSession;
