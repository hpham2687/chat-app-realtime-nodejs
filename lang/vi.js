export const transValidation = {
  email_incorrect: "Email phai co dang example@abc.com",
  gender_incorrect: "Sai gender field,",
  password_incorrect: "mk phai chua it nhat 6 ky tu",
  password_confirmation_incorrect: "mk nhap lai ko dung",
};

export const transErrors = {
  account_in_user: "email nay da duoc su dung",
  account_removed: "tai khoan da bi xoa",
  account_unactive: "tai khoan chua duoc active",
};

export const transSuccess = {
  userCreated: (userEmail) => {
    return `tai khoang <strong> ${userEmail} </strong>. CHeck email to active`;
  },
};
